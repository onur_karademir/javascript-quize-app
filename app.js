const quizData = [
  {
    question: "What is the most used programming language in 2019?",
    a: "Java",
    b: "C",
    c: "Python",
    d: "JavaScript",
    correct: "d",
  },
  {
    question: "Who is the President of US?",
    a: "Florin Pop",
    b: "Donald Trump",
    c: "Ivan Saldano",
    d: "Mihai Andrei",
    correct: "b",
  },
  {
    question: "What does HTML stand for?",
    a: "Hypertext Markup Language",
    b: "Cascading Style Sheet",
    c: "Jason Object Notation",
    d: "Helicopters Terminals Motorboats Lamborginis",
    correct: "a",
  },
  {
    question: "What year was JavaScript launched?",
    a: "1996",
    b: "1995",
    c: "1994",
    d: "none of the above",
    correct: "b",
  },
];

let count = 0;
let score = 0;

let allAnswers = document.querySelectorAll(".answers");
let question = document.getElementById("question");
let submitButton = document.getElementById("submit");

let a_text = document.getElementById("a-text");
let b_text = document.getElementById("b-text");
let c_text = document.getElementById("c-text");
let d_text = document.getElementById("d-text");

setQuize();

function setQuize() {
  var currentQuestion = quizData[count];
  question.innerText = currentQuestion.question;
  a_text.innerText = currentQuestion.a;
  b_text.innerText = currentQuestion.b;
  c_text.innerText = currentQuestion.c;
  c_text.innerText = currentQuestion.c;
}

function answerCheck() {
  let answer = undefined;
  allAnswers.forEach((allAnswers) => {
    if (allAnswers.checked) {
      answer = allAnswers.id;
    }
  });
  return answer;
}
function setBackDefault() {
  allAnswers.forEach((allAnswers) => {
    allAnswers.checked = false;
  });
}

submitButton.addEventListener("click", function () {
  const answer = answerCheck();
  if (answer) {
    if (answer === quizData[count].correct) {
      score++;
    }
    count++;
    setBackDefault();
    if (count < quizData.length) {
      setQuize();
    } else {
      quiz.innerHTML = `
        <h2>You answered correctly at ${score}/${quizData.length} questions.</h2>
        
        <button onclick="location.reload()">Reload</button>
    `;
    }
  }
});
